module Domain
  class Book
    def initialize(book_id, valorations, language)
      @book_id = book_id
      @valorations = valorations
      @language = language
      @creation_date = Time.now.utc
    end

    def yet_stored?
      (@book_id.size > 0)
    end

    def valorations
      @valorations
    end

    def serialize
      {
        book_id: @book_id,
        valorations: serialize_valorations,
        language: @language,
        creation_date: @creation_date,
      }
    end

    def self.from_document(books)
      valorations = []
      book_id = ''
      language = 'es'
      books.each do |element|
        book_id = element['book_id']
        language = element['language']
        valorations = create_valorations(element['valorations'])
      end
      book = self.new(book_id, valorations, language)

      book
    end

    def set_valorations(valorations)
      @valorations = valorations
    end

    private

    def self.create_valorations(valorations)
      created_valorations = []
      valorations.each do |valoration|
        created_valorations << Domain::Valoration.from_document(valoration)
      end

      created_valorations
    end

    def serialize_valorations
      @valorations.map! {|valoration| valoration.serialize}
    end
  end
end
