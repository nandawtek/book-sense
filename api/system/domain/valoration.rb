module Domain
  class Valoration
    attr_reader :valoration

    def initialize(type_id, valoration, counter)
      @type_id = null_defense(type_id)
      @valoration = Integer(valoration) || 0
      @counter = counter
    end

    def self.from_document(document)
      valoration = new(
        document['type_id'],
        document['valoration'],
        document['counter']
      )

      valoration
    end

    def serialize
      {
        type_id: @type_id,
        valoration: @valoration,
        counter: @counter
      }
    end

    def update_value(new_valoration)
      @valoration = new_valoration_value(new_valoration)
      @counter += 1
      self
    end

    def type
      @type_id
    end

    def yet_stored?(new_valoration)
      (Integer(@type_id) == Integer(new_valoration.type))
    end

    private

    def new_valoration_value(new_valoration)
      new_value(new_valoration) / new_counter
    end

    def new_value(new_valoration)
      @valoration * @counter + new_valoration.valoration
    end

    def new_counter
      @counter + 1
    end

    def null_defense(value)
      value || ''
    end
  end
end
