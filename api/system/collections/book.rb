require 'mongo'

module Collections
  class Book
    class << self
      def create(book)
        MongoClient.save(book.serialize)
      end

      def update(book)
        MongoClient.update(book.serialize)
      end

      def retrieve(book_id)
        book = MongoClient.retrieve(book_id)
        Domain::Book.from_document(book)
      end

      private

      class MongoClient
        class << self

          def save(book)
            client[:books].insert_one(book)
          end

          def update(book)
            client[:books].update_one({ 'book_id': book[:book_id] }, { "$set" => book })
          end

          def retrieve(book_id)
            client[:books].find({'book_id': book_id})
          end

          # def filter(filters, language)
          #   client[:valorations]
          #   .aggregate([
          #     {'$match'=> {
          #       '$and' => [
          #         {'language': language},
          #         {'type_id': filters[2]['type_id'], 'valoration' => {"$gt" => 80}},
          #       ]
          #     }},
          #     {"$group" => {"_id" => '$book_id', 'valoration' => {"$avg" => "$valoration"}}},
          #   ])
          # end

          private

          def client
            mongo_uri = ENV['MONGODB_URI']
            Mongo::Logger.logger.level = Logger::INFO

            @client ||= Mongo::Client.new(mongo_uri, { max_pool_size: 5 })
          end
        end
      end
    end
  end
end
