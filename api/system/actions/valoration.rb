require_relative '../services/book'
require_relative '../services/translation'

module Actions
  class Valoration
    class << self
      def create(valorations, language)
        return '' unless valid?(valorations)

        valorations = removed_duplicated_types(valorations)
        message = store_valorations(valorations, language)

        message
      end

      def retrieve_book(id)
        retrieve(id)
      end

      private

      def store_valorations(valorations, language)
        is_saved = Service::Book.create(valorations, language)
        message = Service::Translation.message(is_saved, language)

        message
      end

      def retrieve(id)
        Service::Book.retrieve(id)
      end

      def valid?(valorations)
        validate = true
        valorations.each do |valoration|
          return false if(invalid_request?(valoration))
        end
        validate
      end

      def invalid_request?(valoration)
        return true if valoration['valoration'].nil?
        (
          Integer(valoration['valoration']) < 0 ||
          Integer(valoration['valoration']) > 100 ||
          valoration['book_id'].size == 0 ||
          valoration['type_id'].size == 0
        )
      end

      def removed_duplicated_types(valorations)
        valorations.uniq! { |valoration| valoration['type_id'] }
        valorations
      end

      def uniq_by
        hash, array = {}, []
        each { |i| hash[yield(i)] ||= (array << i) }
        array
      end
    end
  end
end
