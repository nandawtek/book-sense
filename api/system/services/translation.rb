module Service
  class Translation
    class << self
      def message(is_saved, language)
        return success(language) if is_saved
        error(language)
      end

      private

      def success(language)
        translations = {
          "es" => 'Las valoraciones han sido almacenadas'
        }
        translations[language]
      end

      def error(language)
        translations = {
          "es" => 'No se han podido almacenar las valoraciones'
        }
        translations[language]
      end
    end
  end
end
