require_relative '../domain/book'
require_relative '../domain/valoration'
require_relative '../collections/book'

module Service
  class Book
    class << self

      def create(data, language)
        book = retrieve_book(data[0]['book_id'])
        if(book.yet_stored?)
          book = update(book, data)
        else
          book = save(data, language)
        end
        book
      end

      def retrieve(book_id)
        find_by(book_id).serialize
      end

      def retrieve_book(book_id)
        find_by(book_id)
      end

      private

      def save(data, language)
        book = create_book(data, language)
        Collections::Book.create(book)
      end

      def update(book, data)
        book = update_book(book, data)
        Collections::Book.update(book)
      end

      def find_by(book_id)
        Collections::Book.retrieve(book_id)
      end

      def create_book(data, language)
        valorations = create_valorations(data)

        Domain::Book.new(data[0]['book_id'], valorations, language)
      end

      def update_book(book, data)
        data.each do |item|
          update_valorations(book, item)
        end
        book
      end

      def create_valorations(data)
        valorations = Array.new
        data.each do |item|
          valorations << create_valoration(item)
        end
        valorations = sorted_valorations(valorations)

        valorations
      end

      def update_valorations(book, item)
        valorations = create_new_valorations(book, item)
        book.set_valorations(sorted_valorations(valorations))

        book
      end

      def create_new_valorations(book, item)
        is_new_valoration = true
        new_valoration = create_valoration(item)
        valorations = []
        book.valorations.each do |valoration|
          if valoration.yet_stored?(new_valoration)
            is_new_valoration = false
            valoration = valoration.update_value(new_valoration)
          end
          valorations << valoration
        end
        valorations << new_valoration if is_new_valoration

        valorations
      end

      def create_valoration(item)
        first_valoration_counter = 1
        Domain::Valoration.new(item['type_id'], item['valoration'], first_valoration_counter)
      end

      def sorted_valorations(valorations)
        valorations.sort! { |a, b|  a.valoration <=> b.valoration }.reverse!
      end
    end
  end
end
