require 'sinatra/base'
require 'sinatra/cross_origin'
require 'json'

require_relative 'system/actions/valoration'

class App < Sinatra::Base

  configure do
    enable :cross_origin
  end

  before do
     response.headers['Access-Control-Allow-Origin'] = '*'
     content_type 'application/json'
  end

  post '/api/save' do
    data = JSON.parse(request.body.read)

    response = Actions::Valoration.create(data['valorations'], data['language'])

    {message: response}.to_json
  end

  post '/api/retrieve' do
    data = JSON.parse(request.body.read)

    book = Actions::Valoration.retrieve_book(data['book_id'])

    {valorations: book[:valorations]}.to_json
  end

  post '/api/filter' do
    data = JSON.parse(request.body.read)

    books = Actions::Valoration.filter(data['filters'], data['language'])

    {books: books}.to_json
  end

  options "*" do
    response.headers["Allow"] = "GET, POST, OPTIONS"
    response.headers["Access-Control-Allow-Headers"] = "Authorization, Content-Type, Accept, X-User-Email, X-Auth-Token"
    response.headers["Access-Control-Allow-Origin"] = "*"
    200
  end

end
