require_relative '../../system/actions/valoration'
require_relative '../flush/book'

describe 'Valoration' do

  before(:each) do
    @language = 'es'
    Flush::Book.flush
    @type_ids = ['1','2']
    @values = ['90','100']
    @book_id = '1'
  end

  it 'save one item' do
    valorations = add_valorations(@type_ids, @values, @book_id)
    message = Actions::Valoration.create(valorations, @language)

    expect(message).to eq 'Las valoraciones han sido almacenadas'
  end

  it 'respond empty message if book_id is empty' do
    empty_book_id = ''
    valorations = add_valorations(@type_ids, @values, empty_book_id)
    message = Actions::Valoration.create(valorations, @language)

    expect(message).to eq ''
  end

  it 'respond empty message if type_id is empty' do
    type_ids = ['1','']

    valorations = add_valorations(type_ids, @values, @book_id)
    message = Actions::Valoration.create(valorations, @language)

    expect(message).to eq ''
  end

  it 'respond empty message if number is greater than 100' do
    values = ['90','101']

    valorations = add_valorations(@type_ids, values, @book_id)
    message = Actions::Valoration.create(valorations, @language)

    expect(message).to eq ''
  end

  it 'respond empty message if number is smaller than 0' do
    values = ['90','-1']

    valorations = add_valorations(@type_ids, values, @book_id)
    message = Actions::Valoration.create(valorations, @language)

    expect(message).to eq ''
  end

  it 'removed duplicated types' do
    type_ids = ['1','2', '2']
    values = ['90','100', '100']
    valorations = add_three_valorations(type_ids, values, @book_id)

    valoration = Actions::Valoration.send(:removed_duplicated_types, valorations)
    expect(valoration.size).to eq 2
  end

  it 'retrieve types with average valoration' do
    valorations = add_valorations(@type_ids, @values, @book_id)
    Actions::Valoration.create(valorations, @language)
    type_ids = ['1','2','3']
    values = [60, 100, 80]
    valorations = add_three_valorations(type_ids, values, @book_id)
    Actions::Valoration.create(valorations, @language)
    values = [75, 100, 80]
    valorations = add_three_valorations(type_ids, values, @book_id)
    Actions::Valoration.create(valorations, @language)

    book = Actions::Valoration.retrieve_book(@book_id)

    expect(book[:valorations][0][:valoration]).to eq 100
    expect(book[:valorations][1][:valoration]).to eq 80
    expect(book[:valorations][2][:valoration]).to eq 75
    expect(book[:valorations][0][:type_id]).to eq '2'
    expect(book[:valorations][1][:type_id]).to eq '3'
    expect(book[:valorations][2][:type_id]).to eq '1'
    expect(book[:valorations].size).to eq 3
  end

  def add_valorations(type_ids, valorations, book_id)
    [
        {
          'book_id' => book_id,
          'type_id' => type_ids[0],
          'valoration' => valorations[0]
        },
        {
          'book_id' => book_id,
          'type_id' => type_ids[1],
          'valoration' => valorations[1]
        }
    ]
  end
  def add_three_valorations(type_ids, valorations, book_id)
    [
        {
          'book_id' => book_id,
          'type_id' => type_ids[0],
          'valoration' => valorations[0]
        },
        {
          'book_id' => book_id,
          'type_id' => type_ids[1],
          'valoration' => valorations[1]
        },
        {
          'book_id' => book_id,
          'type_id' => type_ids[2],
          'valoration' => valorations[2]
        }
    ]
  end
end
