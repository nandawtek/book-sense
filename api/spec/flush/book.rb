require 'mongo'
module Flush
  class Book
    class << self

      def flush
        client[:books].delete_many
      end

      private

      def client
        mongo_uri = ENV['MONGODB_URI']
        Mongo::Logger.logger.level = Logger::INFO

        @client ||= Mongo::Client.new(mongo_uri, { max_pool_size: 5 })
      end
    end
  end
end
