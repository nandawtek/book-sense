require 'rspec'
require 'json'
require 'rack/test'
require 'securerandom'

require_relative '../../app'
require_relative '../flush/book'

describe 'Api' do

  include Rack::Test::Methods

  def app
    App
  end

  before(:each) do
    @language = 'es'
    Flush::Book.flush
  end

  it 'stores valorations' do
    type_ids = ['1','2']
    valorations = ['90','100']
    book_id = '1'

    add_valorations(type_ids, valorations, book_id)
    response = parse_response

    expect(response['message']).to eq 'Las valoraciones han sido almacenadas'
  end

  it 'get ordered valorations' do
    type_ids = ['1','2']
    valorations = ['90','100']
    book_id = '1'

    add_valorations(type_ids, valorations, book_id)

    get_valorations(book_id)

    response = parse_response

    expect(response['valorations'][0]['valoration']).to eq 100
    expect(response['valorations'][1]['valoration']).to eq 90
  end

  it 'retrieve types with average valoration' do
    type_ids = ['1','2']
    values = ['90','100']
    book_id = '1'
    valorations = add_valorations(type_ids, values, book_id)
    Actions::Valoration.create(valorations, @language)
    type_ids = ['1','2','3']
    values = [60, 100, 80]
    valorations = add_three_valorations(type_ids, values, book_id)
    Actions::Valoration.create(valorations, @language)
    values = [75, 100, 80]
    valorations = add_three_valorations(type_ids, values, book_id)
    Actions::Valoration.create(valorations, @language)

    get_valorations(book_id)

    book = parse_response

    expect(book['valorations'][0]['valoration']).to eq 100
    expect(book['valorations'][1]['valoration']).to eq 80
    expect(book['valorations'][2]['valoration']).to eq 75
    expect(book['valorations'][0]['type_id']).to eq '2'
    expect(book['valorations'][1]['type_id']).to eq '3'
    expect(book['valorations'][2]['type_id']).to eq '1'
    expect(book['valorations'].size).to eq 3
  end

  it 'valorate one valoration one time' do
    type_ids = ['1']
    values = ['90']
    book_id = '1'
    valorations = add_valoration(type_ids, values, book_id)
    type_ids = ['1']
    values = ['90']
    book_id = '1'
    valorations = add_valoration(type_ids, values, book_id)

    get_valorations(book_id)

    book = parse_response

    expect(book['valorations'].size).to eq 1
  end

  xit 'get filtered books' do
    book_id = '3'

    get_filtered

    response = parse_response
    p response

    expect(response['valorations'][0]['valoration']).to eq 99
    expect(response['valorations'][1]['valoration']).to eq 75
    expect(response['valorations'][2]['valoration']).to eq 70
  end

  def add_valoration(type_ids, valorations, book_id)
    valorations = {
      valorations: [
        {
          book_id: book_id,
          type_id: type_ids[0],
          valoration: valorations[0]
        }
      ],
      language: @language
    }.to_json

    post '/api/save', valorations
  end

  def add_valorations(type_ids, valorations, book_id)
    valorations = {
      valorations: [
        {
          book_id: book_id,
          type_id: type_ids[0],
          valoration: valorations[0]
        },
        {
          book_id: book_id,
          type_id: type_ids[1],
          valoration: valorations[1]
        }
      ],
      language: @language
    }.to_json

    post '/api/save', valorations
  end

  def add_three_valorations(type_ids, valorations, book_id)
    valorations = {
      valorations: [
        {
          book_id: book_id,
          type_id: type_ids[0],
          valoration: valorations[0]
        },
        {
          book_id: book_id,
          type_id: type_ids[1],
          valoration: valorations[1]
        },
        {
          book_id: book_id,
          type_id: type_ids[2],
          valoration: valorations[2]
        }
      ],
      language: @language
    }.to_json

    post '/api/save', valorations
  end

  def add_filter_book(language)
    valorations = {
      valorations: [
        {
          book_id: '3',
          type_id: '1',
          valoration: '99'
        },
        {
          book_id: '3',
          type_id: '2',
          valoration: '100'
        },
        {
          book_id: '3',
          type_id: '4',
          valoration: '30'
        }
      ],
      language: language
    }.to_json

    post '/api/save', valorations
  end

  def add_more_valorations(language,valoration)
    valorations = {
      valorations: [
        {
          book_id: '6',
          type_id: '4',
          valoration: '70'
        },
        {
          book_id: '6',
          type_id: '2',
          valoration: valoration
        },
      ],
      language: language
    }.to_json

    post '/api/save', valorations
  end

  def get_valorations(book_id)
    request = {
      book_id: book_id,
    }.to_json

    post '/api/retrieve', request
  end

  def get_filtered
    request = {
      filters:[
        {
          type_id: '1',
          valoration: 80
        },
        {
          type_id: '2',
          valoration: 80
        },
        {
          type_id: '4',
          valoration: 80
        }
      ],
      language: 'es'
    }.to_json

    post '/api/filter', request
  end

  def parse_response
    JSON.parse(last_response.body)
  end
end
