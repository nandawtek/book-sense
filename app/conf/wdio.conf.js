exports.config = {
  specs: [
    './test/e2e/**/*.js'
  ],
  maxInstances: 1,
  host: 'selenium',
  port: 4444,
  baseUrl: 'http://booksense',
  capabilities: [{
    browserName: 'chrome'
  }],
  reporters: ['spec'],
  framework: 'mocha',
  waitforTimeout: 5000,
  mochaOpts: {
      timeout: 20000
  },
}
