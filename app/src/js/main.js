import BookService from './services/bookService'
import ValorationService from './services/valorationService'

import Index from './pages/index/index'
import Search from './pages/shared/search'
import Valorations from './pages/shared/valorations'

import Book from './components/book'
import Valoration from './components/valoration'

new BookService()
new ValorationService()

new Index()

new Book()
new Valoration()

module.exports = {
  Index: Index
};
