import {Bus} from '../infrastructure/bus'
import {Ajax} from '../infrastructure/ajax'


export default class ValorationService {
  constructor() {
    this.client = Ajax
    this.subscribe()
  }

  subscribe() {
    Bus.subscribe("get.filters", this.retrieve.bind(this))
  }

  retrieve(filters) {
    let url = 'filter'
    let callback = this.buildCallback('got.filters')
    this.client.hit(url, filters, callback)
  }

  buildCallback(signal){
    return function(response){
      Bus.publish(signal, response)
    }
  }
}
