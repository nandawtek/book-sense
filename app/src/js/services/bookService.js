import {Bus} from '../infrastructure/bus'
import {Ajax} from '../infrastructure/ajax'


export default class BookService {
  constructor() {
    this.client = Ajax
    this.subscribe()
  }

  subscribe() {
    Bus.subscribe("get.books", this.retrieveBooks.bind(this))
  }

  retrieveBooks(url) {
    let callback = this.buildCallback('got.books')
    this.client.get(url, callback)
  }

  buildCallback(signal){
    return function(response){
      Bus.publish(signal, response)
    }
  }
}
