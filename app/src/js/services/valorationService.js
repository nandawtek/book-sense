import {Bus} from '../infrastructure/bus'
import {Ajax} from '../infrastructure/ajax'


export default class ValorationService {
  constructor() {
    this.client = Ajax
    this.subscribe()
  }

  subscribe() {
    Bus.subscribe("set.valorations", this.save.bind(this))
    Bus.subscribe("get.valorations", this.retrieve.bind(this))
  }

  save(valorations) {
    let url = 'save'
    let callback = this.buildCallback('setted.valorations')
    this.client.hit(url, valorations, callback)
  }

  retrieve(bookId) {
    let data = {}
    data.book_id = bookId
    let url = 'retrieve'
    let callback = this.buildCallback('got.valorations')
    this.client.hit(url, data, callback)
  }

  buildCallback(signal){
    return function(response){
      Bus.publish(signal, response)
    }
  }
}
