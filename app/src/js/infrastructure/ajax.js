import aja from 'aja'

export let Ajax = {
  BASE_URL : 'http://' + process.env.API_HOST + ':4567/api/',

  hit: function (endpoint, data, action){
    aja()
    .method('post')
    .body(data)
    .url(this.BASE_URL + endpoint)
    .on('success', action)
    .go();
  },
  get: function (endpoint, action){
    aja()
    .method('get')
    .url(endpoint)
    .on('success', action)
    .go();
  }

}
