import {Bus} from '../infrastructure/bus'

export default class Valoration {

  constructor(valoration, bookId){
    if(valoration == undefined) return
    this.type_id = valoration.dataset.typeid
    this.valoration = valoration.value
    this.book_id = bookId
  }
}
