export default class Book {

  constructor(data){
    debugger;
    if(data == undefined) return
    this.id = data.id
    this.title = data.volumeInfo.title
    this.authors = data.volumeInfo.authors
    this.description = data.volumeInfo.description || 'No hay descripción'
    this.published = data.volumeInfo.publishedDate
    this.thumbnail = (data.volumeInfo.imageLinks) ? data.volumeInfo.imageLinks.thumbnail : '#'
    this.language = data.volumeInfo.language
  }
}
