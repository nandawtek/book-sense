import {Bus} from '../../infrastructure/bus'
import Search from '../../pages/shared/search'
import Valorations from '../../pages/shared/valorations'
import Filters from '../../pages/shared/filters'
import Book from '../../components/book'


export default class Index {

  constructor(data){
    this.search = new Search()
    this.valorations = new Valorations()
    this.filter = new Filters(this.valorations.createValorations())
    this.subscribe()
  }

  subscribe() {
    Bus.subscribe("got.books", this.show.bind(this))
    Bus.subscribe("setted.valorations", this.message.bind(this))
  }

  show(response) {
    this.cleanItems()
    for(let i = 0 ; i < response.items.length ; i++){
      let book = new Book(response.items[i])
      this.search.render(book)
    }
  }

  message(response) {
    let container = document.getElementsByClassName('message')[0]
    container.innerHTML = ''
    let message = this.prepareMessage(response.message)
    container.appendChild(message)
  }

  prepareMessage(message) {
    let p = document.createElement('p')
    let test = document.createTextNode(message)
    p.appendChild(test)
    return p
  }

  cleanItems() {
    document.querySelector('section.book-items').innerHTML = ''
  }

}
