export default class Modal {

  constructor(modal){
    this.element = document.getElementById(modal)
    this.closeModalEvent()
  }

  closeModalEvent() {
    var span = document.getElementsByClassName("close");
    for(let i = 0 ; i < span.length ; i++){
      span[i].addEventListener('click', this.closeModal.bind(this))
    }
    document.addEventListener('click', this.outsideCloseModal.bind(this))
  }

  showModal(id) {
    this.clean()
    this.element.style.display = "block";
    let send = document.getElementsByClassName('send-valoration')[0]
    send.dataset.bookid = id
  }

  showValorationModal(){
    this.element.style.display = "block";
  }

  showFilterModal(){
    this.element.style.display = "block";
  }

  outsideCloseModal(event) {
    if (event.target == this.element) {
        this.closeModal()
    }
  }

  closeModal() {
    this.element.style.display = "none";
  }

  clean() {
    let progress = document.getElementsByClassName('bar-valoration')
    let number = document.getElementsByClassName('valoration-number')
    for(let i = 0 ; i < progress.length ; i++){
      progress[i].value = 0
    }
    for(let i = 0 ; i < number.length ; i++){
      number[i].value = 0
    }
  }
}
