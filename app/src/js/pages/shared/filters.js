import {Bus} from '../../infrastructure/bus'
import Filter from '../../components/filter'
import Modal from '../shared/modal'

export default class Filters {

  constructor(valorations){
    this.modal = new Modal('filterModal')
    this.modalContent = document.getElementsByClassName('filter-modal-content')[0]
    this.button = document.getElementById('filter')
    this.items = this.createFilters(valorations)
    this.addEvents()
  }

  addEvents(){
    this.button.addEventListener('click', this.showModal.bind(this))
  }

  showModal() {
    this.modal.showFilterModal()
  }
  cleanShow() {
    this.modal.innerHTML = ''
  }


  createFilters(valorations) {
    let lastContext = 'first'
    let section = this.createSection()
    for(let i = 0 ; i < valorations.length ; i++){
      let filter = this.createFilter(valorations[i])
      section.appendChild(filter)
      this.modalContent.appendChild(section)
    }
    let button = this.createSendButton()
    this.modalContent.appendChild(button)
  }

  createSendButton() {
    let button = document.createElement('button')
    let buttonText = document.createTextNode('Filtrar')
    let span = document.createElement('span')
    button.className = 'send-filter'
    button.addEventListener('click', this.collectFilters.bind(this))
    span.className = "fas fa-hands-helping"
    button.appendChild(span)
    button.appendChild(buttonText)
    return button
  }

  createFilter(valoration) {
    let p = document.createElement('p')
    p.className = 'valoration-filter'
    let span = document.createElement('span')
    span.className = 'fa fa-star fa-2x filter-star'
    span.dataset.typeid = valoration.id
    span.addEventListener('click', function() {
      let stars = document.getElementsByClassName('yellow')
      if(this.classList.contains('yellow')){
        this.classList.remove('yellow')
      }else if(stars.length < 3){
        this.classList.add('yellow')
      }
    })
    let input = this.createInput()
    let text = document.createTextNode(valoration.type)
    p.appendChild(input)
    p.appendChild(span)
    p.appendChild(text)
    return p
  }

  createInput() {
    let input = document.createElement('input')
    input.className = 'valoration-filter'
    input.type = 'number'
    input.value = 80
    input.min = 0
    input.max = 100
    return input
  }

  createSection() {
    let section = document.createElement('div')
    section.className = 'filter-section'
    return section
  }


  collectFilters() {
    let stars = document.getElementsByClassName('yellow')
    let collectedData = {}
    collectedData.filters = new Array()
    for(let i = 0 ; i < stars.length ; i++){
      let value = stars[i].parentNode.childNodes[0].value
      let typeId = stars[i].dataset.typeid
      let filter = new Filter(value, typeId)
      collectedData.filters.push(filter)
    }
    collectedData.language = document.getElementById('language').value
    this.modal.closeModal()
    this.sendFilters(collectedData)
  }

  sendFilters(collectedData) {
    if(collectedData.filters.length > 0){
      Bus.publish("get.filters", collectedData)
    }
  }
}
