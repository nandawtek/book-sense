import {Bus} from '../../infrastructure/bus'
import Valoration from '../../components/valoration'
import Modal from '../shared/modal'

export default class Valorations {

  constructor(){
    this.items = this.createValorations()
    this.modal = document.getElementsByClassName('modal-content')[0]
    this.showModal = document.getElementsByClassName('showed-valorations')[0]
    this.add()
    this.subscribe()
  }

  subscribe() {
    Bus.subscribe("got.valorations", this.render.bind(this))
  }

  add() {
    this.createSections()
    this.createSendButton()
    this.makeProgressClickable()
  }

  render(response) {
    this.cleanShow()
    let modal = new Modal('showValorations')
    let section = this.createSection()
    for(let i = 0 ; i < response.valorations.length ; i++){
      let valoration = this.createShowValorations(i, response.valorations[i])
      section.appendChild(valoration)
      this.showModal.appendChild(section)
    }
    modal.showValorationModal()
  }

  cleanShow() {
    this.showModal.innerHTML = ''
  }

  makeProgressClickable(){
    let progress = document.getElementsByClassName('clickable')
    for (let i = 0 ; i < progress.length ; i++){
      progress[i].addEventListener('click', function(event) {
        this.parentNode.childNodes[2].value = parseInt(event.offsetX / 2)
        this.value = parseInt(event.offsetX / 2)
      })
    }
  }

  createSections() {
    let lastContext = 'first'
    let section = ''
    for(let i = 0 ; i < this.items.length ; i++){
      let valoration = this.createValoration(i)
      if(lastContext != this.items[i].context){
        section = this.createSection()
      }
      section.appendChild(valoration)
      this.modal.appendChild(section)
      lastContext = this.items[i].context
    }
  }

  createSendButton() {
    var button = document.createElement('button')
    var buttonText = document.createTextNode('Enviar')
    var span = document.createElement('span')
    button.className = 'send-valoration'
    button.addEventListener('click', this.collectValorations.bind(this))
    span.className = "fas fa-hands-helping"
    button.appendChild(span)
    button.appendChild(buttonText)
    this.modal.appendChild(button)
  }

  createValoration(index) {
    let valoration = document.createElement('div')
    let h2 = this.createValorationLine(index)
    valoration.appendChild(h2)
    return valoration
  }

  createValorationLine(index, valoration) {
    let h2 = document.createElement('h2')
    let type = this.createType(index)
    let progress = this.createProgress(index, 0)
    let input = this.createInput()
    input.addEventListener('change', function() {
      progress.value = this.value
    })
    h2.appendChild(type)
    h2.appendChild(progress)
    h2.appendChild(input)
    return h2
  }

  createShowValorations(index, valoration) {
    let h2 = document.createElement('h2')
    h2.className = 'relative'
    let type = this.createValorationName(valoration)
    let progress = this.createProgress(index, valoration['valoration'], 'show-bar')
    h2.appendChild(type)
    h2.appendChild(progress)
    return h2
  }

  createType(index) {
    let span = document.createElement('span')
    let type = document.createTextNode(this.items[index].type)
    span.appendChild(type)
    return span
  }

  createValorationName(valoration) {
    let span = document.createElement('span')
    span.className = 'valoration-name'
    let type = document.createTextNode(this.getType(valoration['type_id']))
    span.appendChild(type)
    return span
  }

  createProgress(index, value, className = 'bar-valoration clickable') {
    let progressbar = document.createElement('progress')
    progressbar.className = className
    progressbar.dataset.typeid = this.items[index].id
    progressbar.value = value
    progressbar.max = 100
    return progressbar
  }

  createInput() {
    let input = document.createElement('input')
    input.className = 'valoration-number'
    input.type = 'number'
    input.value = 0
    input.min = 0
    input.max = 100
    return input
  }

  createSection() {
    let section = document.createElement('div')
    section.className = 'valoration-section'
    return section
  }


  collectValorations() {
    let valorations = document.getElementsByClassName('bar-valoration')
    let bookId = document.getElementsByClassName('send-valoration')[0].dataset.bookid
    let collectedData = {}
    collectedData.valorations = new Array()
    for(let i = 0 ; i < valorations.length ; i++){
      let valoration = this.isCorrect(valorations[i], bookId)
      if(valoration){
        collectedData.valorations.push(valoration)
      }
    }
    collectedData.language = document.getElementById('language').value
    this.closeModal()
    this.sendValorations(collectedData)
  }

  isCorrect(valoration, bookId){
    if(valoration.value > 0 && valoration.value <= 100){
      let result = new Valoration(valoration, bookId)
      return result
    }
    return false
  }

  sendValorations(collectedData) {
    if(collectedData.valorations.length > 0){
      Bus.publish("set.valorations", collectedData)
    }
  }

  closeModal() {
    document.getElementById('valorationModal').style.display = "none";
  }

  createValorations() {
    return [
      {
        'id': '1',
        'type': 'Novela',
        'context': 'style',
      },
      {
        'id': '2',
        'type': 'Histórico',
        'context': 'style',
      },
      {
        'id': '3',
        'type': 'Divertido',
        'context': 'other',
      },
      {
        'id': '4',
        'type': 'Lascivo',
        'context': 'other',
      }
    ]
  }
  getType(id) {
    const types = {
      "1": "Novela",
      "2": "Historico",
      "3": "Divertido",
      "4": "Lascivo",
    }
    return types[id]
  }
}
