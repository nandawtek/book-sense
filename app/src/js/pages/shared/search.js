import {Bus} from '../../infrastructure/bus'
import Modal from '../shared/modal'

export default class Search {

  constructor(){
    this.modal = new Modal('valorationModal')
    this.search = document.getElementById('search')
    this.find = document.getElementById('find')
    this.fields = '&fields=items(id,volumeInfo(title,authors,publishedDate,description,imageLinks/thumbnail,language))'
    this.events()
  }

  events() {
    this.find.addEventListener('click', this.executeSearch.bind(this))
    this.search.addEventListener('keyup', this.searchBook.bind(this))
  }

  searchBook(event) {
    if(event.keyCode == 13){
      this.executeSearch();
    }
  }

  render(book) {
    let section = document.querySelector('section.book-items')
    let item = this.create(book)
    section.appendChild(item)
  }

  executeSearch() {
    let content = this.search.value
    let lang = '&langRestrict=' + document.getElementById('language').value
    let url = 'https://www.googleapis.com/books/v1/volumes?q=' + content + lang + this.fields
    this.search.value = ''
    Bus.publish("get.books", url)
  }

  create(book) {
    let div = document.createElement("div")
    div.className = 'book-item'
    let valoration = this.createValorationButton(book.id, 'valoration-button', 'Valorar')
    this.addValorationsEvent(valoration, book.id)
    let see = this.createValorationButton(book.id, 'see-valorations', 'Ver Valoración')
    this.addSeeValorationsEvent(see, book.id)
    let h3 = this.createTitle(book)
    let date = this.createDate(book)
    let p = this.createDescription(book)
    let authors = this.createAuthors(book)
    div.appendChild(valoration)
    div.appendChild(see)
    div.appendChild(h3)
    div.appendChild(date)
    div.appendChild(p)
    div.appendChild(authors)
    return div
  }

  createValorationButton(bookId, className, content) {
    let button = document.createElement('button')
    button.className = className
    button.dataset.bookid = bookId
    let text = document.createTextNode(content)
    button.appendChild(text)
    return button
  }

  addValorationsEvent(button, bookId) {
    button.addEventListener('click', function(){this.modal.showModal(bookId)}.bind(this))
  }

  addSeeValorationsEvent(button, bookId) {
    button.addEventListener('click', function(){Bus.publish("get.valorations", bookId)})
  }

  createTitle(book) {
    let h3 = document.createElement('h3')
    h3.className = 'title'
    let title = document.createTextNode(book.title);
    h3.appendChild(title)
    return h3
  }

  createDate(book) {
    let date = document.createElement('h5')
    let publishedDate = document.createTextNode(book.published)
    date.appendChild(publishedDate)
    return date
  }

  createDescription(book) {
    let p = document.createElement('p')
    let description = document.createTextNode(book.description);
    let image = document.createElement('img')
    image.src = book.thumbnail
    p.appendChild(image)
    p.appendChild(description)
    return p
  }

  createAuthors(book) {
    let authors = document.createElement('h4')
    authors.className = 'book-authors'
    if(book.authors == undefined) return authors
    for(let i = 0 ; i < book.authors.length ; i++){
      let author = ''
      if(i > 0){
        author = document.createTextNode(' - ' + book.authors[i])
      }else{
        author = document.createTextNode(book.authors[i])
      }
      authors.appendChild(author)
    }
    return authors
  }

}
