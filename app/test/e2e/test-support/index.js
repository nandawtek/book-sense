class Index{

  constructor(){
    browser.url('/')
  }

  fillSearch(value){
    let search =  $('#search')
    search.setValue(value)
  }

  selectLanguage(selectedLanguage){
    let language =  $('#language')
    language.selectByValue(selectedLanguage)
  }

  sendValoration(book){
    this.fillSearch(book)

    this.pressEnter()

    this.pressValoration()

    this.fillValoration()

    this.pressValorationSubmit()
  }

  pressEnter(){
    let keyEnter="\uE007"
    browser.keys(keyEnter)
  }

  pressSubmit(){
    $('#find').click()
  }

  pressValoration(){
    browser.waitForVisible('.valoration-button', 10000);
    browser.click('.valoration-button');
  }

  pressSeeValorations(){
    browser.waitForVisible('.see-valorations', 10000);
    browser.click('.see-valorations');
  }

  fillValoration(){
    $('.valoration-number').setValue(99)
  }

  pressValorationSubmit(){
    $('.send-valoration').click()
  }

  message() {
    browser.waitForVisible('div.message p', 10000);
    return browser.getText('div.message p')
  }

  valoration() {
    browser.waitForVisible('h2 span.valoration-name', 10000);
    return browser.getText('h2 span.valoration-name')
  }

  isPresent(book){
    let list = this.chargedBook()
    for(let i = 0 ; i < list.length ; i++){
      if(list[i] == book) return true
    }
    return false
  }

  chargedBook(){
    browser.waitForVisible('h3.title', 10000);
    return browser.getText('h3.title')
  }
}
module.exports = Index
