const expect = require('chai').expect
const Index = require('./test-support/index')

describe('Search', () => {

  it ("books", () => {
    const index = new Index()

    index.fillSearch('Los colmillos del tropico')

    index.pressEnter()

    let isPresent = index.isPresent('Los colmillos del trópico')

    expect(isPresent).to.eq(true)
  })

  it ("authors", () => {
    const index = new Index()

    index.fillSearch('Enrique Vaque')

    index.pressSubmit()

    let isPresent = index.isPresent('Los colmillos del trópico')

    expect(isPresent).to.eq(true)
  })

  it ("for language", () => {
    let aleman = 'de'
    const index = new Index()

    index.fillSearch('King kong')

    index.selectLanguage(aleman)

    index.pressSubmit()

    let isPresent = index.isPresent('King Kong, das Geheimschwein - Ein Unterrichtsentwurf')

    expect(isPresent).to.eq(true)
  })

})
describe('Valoration', () => {

  it ("save success", () => {
    const index = new Index()

    index.sendValoration('Los colmillos del tropico')

    expect(index.message()).to.eq('Las valoraciones han sido almacenadas')
  })

  it ("show saved valorations", () => {
    const index = new Index()

    index.sendValoration('Los colmillos del tropico')

    index.fillSearch('Enrique Vaque')

    index.pressSubmit()

    index.pressSeeValorations()

    expect(index.valoration()).to.eq('Novela')
  })
})
